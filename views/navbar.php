<?php
/*
 * Copyright 2015 Simple Design <Rob.Xcog at xcogstudios@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
?>
<nav class="navbar navbar-default nav-justified shadowed" id="slNavbar">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <span class="row">

            <div class="navbar-header">
                 <span class="col-lg-4">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#slNavCollapse" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               
                    <span class="navbar-brand navbar-logoWrapper">
                        <div id="sl-navbar-Logo" class="nav-logoContainer" >
                            <img id="nav-logoShield" class="nav-logoShield shadowed"src="/img/shield.svg">
                            <img id="nav-logoText" class="nav-logoText "src="/img/text.svg">

                        </div>
                    </span>
                </span>
            </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse slNavbar" id="slNavCollapse">
                    <span class=''>
                        <span class="nav-navMenuWrapper">
                            <div class="nav-navMenuContainer" >
                                <ul class="nav nav-navbar slNav" id="nav-menu">
                                    <li><a class="hvr-underline-from-center nav-item" data-toggle="modal" data-target="#slider-modal"> Get Quoted </a></li>
                                    <li><a class="hvr-underline-from-center nav-item" href="#serv-a">Services</a></li>
                                    <li><a class="hvr-underline-from-center nav-item" href="#footer-a">Partners</a></li>
                                    <li><a class="hvr-underline-from-center nav-item" href="#quotes-a">Testimonies</a></li>
                                    <li><a class="hvr-underline-from-center nav-item" href="#about-a">About Us</a></li>                
                                </ul>
                            </div>
                        </span>
                    </span>
                </div><!-- /.navbar-collapse -->
        </span> <!-- row -->
    </div><!-- /.container-fluid -->
</nav>
<!--
<script src='//www.google.com/jsapi' type='text/javascript'></script>
<script type='text/javascript'>

  (function() {
    var cx = '014291306314433072867:tg5hl4tfahw';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
-->