<?php
/*
 * Copyright 2015 Simple Design <Rob.Xcog at xcogstudios@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
?>
<span role="header" class="text-center formHeader text-nowrap"> SEND US A <br>
    <i class="fa fa-envelope fa-3x hvr-icon-buzz-out"></i>
</span>
<br>
<img class="slLineBreak "src="img/lineBreak.png">
<div id="form-messages"></div>
<form class="center-block" id="ajax-contact" method="post" action="/backend/formValidator.php" >
      <h3>Contact Information</h3>
    
    <i class="fa fa-user fa-4x"></i>
    <br>
    <span class="form-inline">
        <div class="form-group">
            <br>
            <input type="text" class="form-control" name="firstName"  size="40" required placeholder="First Name">
        </div>
        <div class="form-group">
            <br>
            <input type="text" class="form-control" name="lastName"  size="40"placeholder="Last Name">
        </div>
        <span class="error"><?php echo $nameErr; ?></span>
        <br>
    </span>
    <br>
    <span class="">
     <i class="fa fa-send fa-4x"></i>
     <br>
        <div class="form-group">
            <label for="slInputEmail">
            </label>
            <br>
            <input ng-pattern="/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i" type="email" class="form-control" name="email" id="slInputEmail"  placeholder="Y O U R @ E M A I L . C O M">
       <br>
        </div>
                          <i class="fa fa-mobile fa-5x "></i>
            <br>
            
            

        <div class="form-group">
           
            <input ng-model="val" ng-pattern="/^\d+$/" name="anim" class="my-input"
         aria-describedby="inputDescription"type="number" class="form-control" id="slInputPhone" name="phone"  placeholder="5 5 5 - 8 6 7  - 5 3 0 9">
        </div>
        <span class="error"> <?php echo $nameErr; ?></span>
        <br><br>
    </span>
    <br>
    
    <span class="form-inline">
        
    <br>

    <i class="fa fa-pencil fa-3x"></i>
    <br>
    <textarea name="message" rows="8" cols="90" placeholder="A brief discription on how we may help you."><?php echo $comment; ?></textarea>
        <button id="locationToggleBtn" type="button" onclick="getLocation()" class="btn btn-danger" >Add Your Location <i class="fa fa-map-marker fa-5x"></i></button>

    </span>
    
    <br><br>

    <br><br>
    <input type="submit" class="btn btn-primary slSubmitFormBtn" name="submit" value="Submit"> 
    <input type="reset" class="btn btn-info" name="reset" value="reset"> 
    <input type="hidden" id='geoLocationInput' name='geoLocation' >
    <input type="hidden" name="messageSubmit" value="Submit">

</form>
<div id='map'> </div>
<script>

// Note: This example requires that you consent to location sharing when
// prompted by your browser. If you see the error "The Geolocation service
// failed.", it means you probably did not give permission for the browser to
// locate you.



    var slGPSMessage = document.getElementById("geoLocationMessage");
    var slGPSInput = document.getElementById("geoLocationInput");
    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            slGPSMessage.innerHTML = "Geolocation is not supported by this browser.";
        }
    }

    function showPosition(position) {
        slGPSMessage.innerHTML = "Latitude: " + position.coords.latitude +
                "<br>Longitude: " + position.coords.longitude;
        slGPSInput.setAttribute("value", "https://www.google.com/maps/?q=" + position.coords.latitude +
                "," + position.coords.longitude);
    }





</script>

<!--
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      document.getElementById("geoLocationMessage").innerHTML = xhttp.responseText();
    }
  }
  xhttp.open("GET", "ajax_info.txt", true);
  xhttp.send();
}
</script>
-->
