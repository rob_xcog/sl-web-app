<?php
/*
 * Copyright 2015 Simple Design <Rob.Xcog at xcogstudios@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
?>
<a id="services"></a>
<div class="services-header text-center">
    <span class="services-preTitle">Smiling Locksmith's </span>
    <h2 class="services-title hvr-underline-reveal">Services</h2>
</div>

<div class="container">
    <div class="row">
        <span ng-repeat="service in services track by $index" >
            <span class="serviceWrapper col-lg-2 col-md-4 col-sm-12">           
               <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#{{service.viewID}}-collapse" aria-expanded="false" aria-controls="collapseExample">

                   <span class="fa-stack fa-5x pull-left">
                        <i class="fa fa-circle-o fa-stack-2x fa-inverse serviceIcons"></i>
                        <i class="fa fa-{{service.icon}} fa-stack-1x fa-inverse serviceIcons"></i>            
                    </span>
               </button>
                <h3>{{service.header}} </h3>
                <p class="service-description">
                    {{service.description}}
                </p>
                </span>
                <div class="collapse" id="{{service.viewID}}-collapse">
                <span class="well">

                    <div class="container"> 
                        <div class="row">
                            <div id="item{{$index}}" class="itemWrapper" ng-repeat="item in service.selctions">
                            <div id="lockedOut" class="col-sm-offset-1 col-sm-4">
         <div class="view view-tenth">        
                                <img width="100%" height="auto" src="{{item.imageURL}}">
                    <div class="mask">
                        <h2>{{item.header}}</h2>
                        <p>{{item.description}}</p>
                        <a href="#" class="info">Request Quote</a>
                    </div>
         </div>
    </div>
                            </div>
                        </div>
                        
                    </div>
                </span>
                </div>
        </span>
    </div>


</div>
