<?php
/*
 * Copyright 2015 Simple Design <Rob.Xcog at xcogstudios@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
?>
<div class="splash-contianer" >
    
    
    <div class="parallax">
        <div class="bg__splash" data-scroll-speed="-0.3">

</div>
    <div class="splash-topBannerWrapper">  

        <span class="splash-topBanner container" ng-controller="splashCtrl"id="splash-BannerContainer">
            <span class="row">
                <span class="col-sm-4 ">
                    <a class="text-nowrap hvr-underline-from-left splash-topBannerAnc " ng-click="btnClick(Automotive)"  id="splashAutomotiveA" href="#automotive-a" ><img src="../img/AUTOMOTIVE.svg"></a> 
                </span>             
                <span class="col-sm-4 ">
                    <a class="text-nowrap hvr-underline-from-center splash-topBannerAnc" id="splashResidentialA"  href="#services" ><img  id="splashResSVG" src="../img/RESIDENTIAL.svg"></a>
                </span>
                <span class="col-sm-4 ">
                    <a class="text-nowrap hvr-underline-from-right splash-topBannerAnc" id="splashCommercial"  href="#services" ><img src="../img/COMMERCIAL.svg"></a>     
                </span>                                 
            </span>
           
        </span>
    </div>
    
 
    
    <div class="splash-logoWrapper" >
        <span id="splash-logoContainerObj" class="splash-logoContainer" >            
           
            <div class="splash-nameWrapper" >
        <span class="splash-nameContainer">
               <img id="logo-text" class="splash-logoText" src="/img/text.svg">
        </span>
            </div>
            <div class="splash-shieldWrapper">
             <span class="splash-shieldContainer">
                <img id="logo-shield "class="splash-shield shadowed"src="/img/shield.svg">                   
        
            </span>
            </div>    
                
        </span>
    </div>
    
        <span class="splash-footerWrapper">
        <span class="splash-footerContainer container">
            
                <span class="row " id="splash-footerRow">
                    <span class="col-xs-6">
                        <a class="splash-footerNumberA hvr-overline-from-left text-nowrap" href="tel:9713001177" ><img src="../img/phoneNumber.svg"> </a>
                    </span>
                    <span class="col-xs-6">
                        <a class="splash-footerEmailA hvr-overline-from-right text-nowrap" href="mailto:smilinglocksmith@yahoo.com" > <img src="../img/email.svg"> </a>
                    </span>
                </span>
        </span>
    </span>


    <div class="splash-blurbWrapper">

        <span class="splash-blurb hvr-bob">
            <span class="splash-blurbContianer">
                
                <span class="splash-blurbText text-nowrap headerTextBlock1">Portland Metro's</span>
                <span class="splash-blurbText headerTextBlock2 text-nowrap">Lock and Key</span> 
                <span class="splash-blurbText headerTextBlock3 text-nowrap">Security Solutions</span>
            
            </span>
        </span>
    </div>        

   </div>

</div>