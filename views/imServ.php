<?php
/*
 * Copyright 2015 Simple Design <Rob.Xcog at xcogstudios@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
?>

<div id="imServ-containerEle" class="container">
    <div class="row">
        <span class="imServ-header">

            <span class="col-xs-12 col-md-8">
                <h1 class="imServ-title shadowed">IMMEDIATE SERVICE</h1>
            </span>
            <span class="col-xs-12 col-sm-4">

                <h3>SMILING SERVICE 24/7</h3>
                <p class="imServ-subTitle shadowed"> Also you can send your devices location in your message and your technician will know how to get to you. . </p>

            </span>
        </span>
    </div> <!-- row -->
    <div class="row">


        <span class="col-sm-12 col-md-5">
                                <span class="row">

            <span class="imServ-phoneContainer hvr-shutter-out-vertical">

                <a href="tel:9713001177"> 

                        <span class="col-sm-4 col-md-12">
                            <span id="imServ-CALLNOW" class="imServ-text">CALL NOW</span>
                        </span>
                        <span class="col-sm-4 col-md-12">
                            <span class="imServ-iconWrapper">   

                                <span class="fa-stack fa-4x">
                                    <i class="fa fa-square-o fa-stack-2x"></i>
                                    <i class="fa fa-mobile fa-stack-1x" id="emergencyServices-mobile"></i>
                                </span>
                            </span>
                        </span>
                        <span class="col-sm-4 col-md-12">
                            <span class="imServ-text emergencyServiceNumber">( 9 7 1 ) 3 0 0 - 1 1 7 7 </span> 
                        </span>
                    </span> <!-- row -->

                </a>
            </span>
        </span>
        <span class="col-sm-12 col-md-2">
            <span id="imServ-OR" class="imServ-text">-OR-</span>
        </span>
        <span class="col-sm-12 col-md-5">
            <span class="row">
                <span class="imServ-iMContainer hvr-shutter-out-horizontal">
                    <a id="" href="#messenger" class="">

                        <span class="col-sm-4 col-md-12">
                            <span class="text-nowrap imServ-text"> INSTANT MESSAGE</span>
                        </span>  <!-- col -->
                        <span class="col-sm-4 col-md-12">
                            <span class="fa-stack fa-4x">
                                <i class="fa fa-square-o fa-stack-2x"></i>
                                <i id="emergencyServices-map-marker" class="fa fa-map-marker fa-stack-1x"></i>
                            </span>
                        </span>  <!-- col -->
                        <span class="col-sm-4 col-md-12">
                            <span class="text-nowrap imServ-text">WITH GEO-LOCATION </span>
                        </span>  <!-- col -->
                    </a>
                </span>        
            </span>
        </span> <!-- col -->
    </div>
</div> <!-- contianer -->

