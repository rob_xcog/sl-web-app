<?php
/*
 * Copyright 2015 Simple Design <Rob.Xcog at xcogstudios@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
?>
<div class="flexslider" ng-controller="sl-sliderCtrl">
    <ul class="slides">
        <li ng-repeat="slide in slides" id="{{slide.slideID}}" >
            <img src="{{slide.imgURL}}" >
            <span class="slider-blurbWrapper"> 

                <span class="slider-blurbContainer">
                    <span class=" slider-blurbA shadowed" ><h2>{{slide.textLineA}}</h2></span>
                    <span class=" slider-blurbB shadowed">{{slide.textLineB}}</span> 
                    <span class=" slider-blurbC shadowed"> {{slide.textLineC}}</span>
                    <a class="btn btn-primary slider-blurbButton shadowed" data-toggle="modal" data-target="#slider-modal">Get A Free Quote</a>
                </span>
            </span>
        </li>
    </ul>
</div>

<script>

    $(document).ready(function () {
        $('.slflexButton a').hover(function () {
            $(this).stop().animate({'opacity': '0'}, 500);
        }, function () {
            $(this).stop().animate({'opacity': '1'}, 500);
        });
    });


</script>

