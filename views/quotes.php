<?php
/*
 * Copyright 2015 Simple Design <Rob.Xcog at xcogstudios@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
?>

<div class="container">

    <div class="row">
        <h2 class="quotes-header">S M I L I N G &nbsp; C L I E N T S</h2>  
        <span class="quotes-headerText"> Smiling Locksmith is a professional and effective. Providing effective solutions for our clients are what we strive to
            make better everyday and we appreciate every word of encouragement and suggestions you all have given us. </span>
    </div> <!-- row -->
    <div ng-controller="sl-quotesCtrl" class="row"> 

        <span class="col-sm-12 col-md-6 col-lg-2" ng-repeat="quote in quotes">

            <div class="quotes-quoteWrapper">
                <span class="quote-{{quote.name}} hvr-bubble-float-bottom">
                    <p >
                        <i class="fa fa-quote-left fa-3x fa-pull-left fa-border"></i>
                        {{quote.quote}}            
                    </p>
                    <span class="quotes-{{quote.name}}">
                        -{{quote.name}}
                        
                        
                    </span>
                    <span class="rating">
<span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
                    </span>
                </div>
                
            </span>
       
    </div> <!-- row -->
   
</div> <!-- end of Testimonial Fluid Contaitner -->
