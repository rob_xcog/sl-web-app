<?php

/* 
 * Copyright 2015 Simple Design <Rob.Xcog at xcogstudios@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
//php init
date_default_timezone_set('America/Los_Angeles');
//module init
require '../vendor/autoload.php';
// parse init
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParsePush;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseException;
use Parse\ParseAnalytics;

ParseClient::initialize('0onjpoJmqMiZlyPohoA7doC5ibCpiEG6turvoLLM', 'AUPdQhZf46OWe7vrg0GW0lapfYv9hMKwOOofvZwY', 'fs5ooCrlKltlZl3SXKKG1wcSikPbJd61zPVBQc6b');

// mailgun init
use Mailgun\Mailgun;

# Instantiate the client.
$mgClient = new Mailgun('key-4cbfa9c9bdca219d0250259ce036a125');
$domain = "sandboxce8e09029fa140fca1e6a47089d01e6e.mailgun.org";

$name = $_POST['firstName'] . $_POST['lastName'];
$messageID = "";

        if ($_POST['messageSubmit'] == "Submit") {
            $errorMessage = "";
            if (empty($name)) {
                $errorMessage = "Please provide your first and last name";
            }; 
            if (empty($_POST['phone'])){
                $errorMessage = "Please provide at least one way we can talk with you. ";
            }
            if (empty($_POST['email'])){
                $errorMessage = "No email was provied";
            }
            if (!empty($errorMessage)) {
                echo("<p>There was an error:</p>\n");
                echo("<ul>" . $errorMessage . "</ul>\n");
            } else {
               
                $messageData = new ParseObject("message");

                if (!empty($_POST['geoLocation'])) {
                   
                $geoLocationURL = $_POST['geoLocation'];
            $messageData->set("geoLocation", $geoLocationURL);
                }  else {
                   $messageData->set("geoLocation", "Not Supplied");

                }
                $messageData->set("name", $_POST['name']);
                $messageData->set("phone", $_POST['phone']);              
                $messageData->set("email", $_POST['email']);
                $messageData->set("message", $_POST['message']);
                try {
  $messageData->save();
  $messageID = $messageData->getObjectId();
  echo "Your message's ID is"  . $messageID;
} catch (ParseException $ex) {  
  // Execute any logic that should take place if the save fails.
  // error is a ParseException object with an error code and message.
  echo 'Failed to create new object, with error message: ' . $ex->getMessage();
}
            

}
            }
        
        
    

    // My modifications to mailer script from:
    // http://blog.teamtreehouse.com/create-ajax-contact-form
    // Added input sanitizing to prevent injection

    // Only process POST reqeusts.
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Get the form fields and remove whitespace.
        $name = strip_tags($name);
				$name = str_replace(array("\r","\n"),array(" "," "),$name);
        $phone = strip_tags(trim($_POST["phone"]));
        $phone = str_replace(array("\r","\n"),array(" "," "),$phone);
        
        $email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);
        $message = trim($_POST["message"]);

        // Check that data was sent to the mailer.
        if ( empty($name) OR empty($message) OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // Set a 400 (bad request) response code and exit.
            http_response_code(400);
            echo "Oops! There was a problem with your submission. Please complete the form and try again.";
            exit;
        }
        if (!empty($_POST['geoLocation'])) {
            $geoLocationURL = $_POST['geoLocation'];
            $message = "$message\n\n"
                    . " GeoLocation Map URL : $geoLocationURL ";
        }else{
            $message = "$message\n\n GeoLocation: no geolocation supplied";
        }

        // Set the recipient email address.
        // FIXME: Update this to your desired email address.
        $recipient = "simdevrob@gmail.com";

        // Set the email subject.
        $subject = "New contact from $name";

        // Build the email content.
        $email_content = "Name: $name\n";
        $email_content .= "Email: $email\n";
        $email_content .= "Phone: $phone\n\n";
        $email_content .= "Message:\n$message\n";

        // Build the email headers.
        $email_headers = "From: $name <$email>";

        // Send the email.
        if (mail($recipient, $subject, $email_content, $email_headers)) {
            // Set a 200 (okay) response code.
            http_response_code(200);
            
# Make the call to the client.
$result = $mgClient->sendMessage($domain, array(
    'from'    => 'Excited User <mailgun@sandboxce8e09029fa140fca1e6a47089d01e6e.mailgun.org>',
    'to'      => 'Baz <5037096353@mms.att.net>',
    'subject' => 'Web Message From '.$_POST['name']." Message ID#: $messageID\n",
    'text'    => "From: $name\nContact Phone: $phone\n" 
                 . "Contact Email: $email\n"
                 . "Message ID: $messageID\n\n"
                 . "Message:\n $message"
));

            echo "Thank You! Your message has been sent.";
        } else {
            // Set a 500 (internal server error) response code.
            http_response_code(500);
            echo "Oops! Something went wrong and we couldn't send your message.";
        }

    } else {
        // Not a POST request, set a 403 (forbidden) response code.
        http_response_code(403);
        echo "There was a problem with your submission, please try again.";
    }
        
    
 
        
