<?php
date_default_timezone_set('America/Los_Angeles');
require 'vendor/autoload.php';

use Parse\ParseClient;

ParseClient::initialize('0onjpoJmqMiZlyPohoA7doC5ibCpiEG6turvoLLM', 'AUPdQhZf46OWe7vrg0GW0lapfYv9hMKwOOofvZwY', 'fs5ooCrlKltlZl3SXKKG1wcSikPbJd61zPVBQc6b');
?>
<!doctype html>
<html class="no-js" lang="eng" ng-app="slApp">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Smiling Locksmith, Portland Metro's Lock and Key Manufacturing and Security Solutions.</title>
        <meta name="description" content="">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php include_once 'iconLinks.php'; ?>


        <!-- Local Resources  -->

        <script src="js/jquery-2.1.4.min.js" type="text/javascript"></script>
        <link href="vendor/components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/twbs/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <!-- <link href="node_modules/angular/angular-csp.css" rel="stylesheet" type="text/css"/> -->


        <script src="vendor/twbs/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/vendor/jquery.sticky.js"></script>

        <script>

            $(document).ready(function () {
                $("#slNavbar").sticky({topSpacing: 0});
            });
        </script>
        <link rel="stylesheet" href="styles/stylesheets/vendor/hover.css">
        <!-- SimDev Styles -->


        <script src="js/vendor/navbarZIndexer.js" type="text/javascript"></script>
        <script src="js/formHandler.js" type="text/javascript"></script>
        <script src="js/flexslider.js" type="text/javascript"></script>
        <script src="node_modules/angular/angular.min.js" type="text/javascript"></script>

        <!-- SimDev App Instantiation -->

        <script src="js/app.js" type="text/javascript"></script>

        <link rel="stylesheet" href="styles/stylesheets/styles.css"/>


    </head>
    <body  ng-controller="sl-serviceCtrl">

        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->


            <header id="headerE">
            <a id="pageTop"></a>
            <div class="splash-wrapper  ">
                <?php include_once 'views/splash.php'; ?>
            </div>

        </header>   
       
        <section id="content" class="contentWrapper">
            <?php include_once 'views/navbar.php'; ?>
            <div class="flex-wrapper" >
                <?php include_once 'views/flexSlider.php'; ?>
                <div class="flex-transWrapper">
                    <img id="flex-transTop"  src="/img/bottomTransition.svg" />
                </div>
            </div>
            <div class="imServ-wrapper">
                <div class="" >

                    <?php include_once 'views/imServ.php'; ?> 
                </div>
            </div>

            <div class="flex-transWrapper">
                <img id="flex-transBottom"  src="/img/pageTrans-bottom.svg" />
            </div>
            <div class="services-wrapper">                   
                <a id="serv-a"></a>
                <?php include_once 'views/services.php'; ?>
            </div>
            <div class="iM-wrapper">

                <a id="iM-a" ></a>

                <?php include 'views/iM.php'; ?>

            </div>
            <div class="quotes-wrapper">
                <a id="quotes-a" ></a>
                <?php include_once 'views/quotes.php'; ?> 

            </div>
            <div class="about-wrapper">
                <a id="about-a" ></a>

                <?php include_once 'views/about.php'; ?>
            </div>
            <a id="footer-a" ></a>
        </section>
        <footer id="footerE">
            <div class="footer-wrapper">


                <?php include_once 'views/footer.php'; ?> 
            </div>
        </footer>
        <div id="callButton">
            <a  href="tel:9713001177"><i class="fa fa-phone fa-5x"></i><span id="sl-callButtonContent"></span></a>
        </div>                

        <script>
                    (function (i, s, o, g, r, a, m) {
                        i['GoogleAnalyticsObject'] = r;
                        i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                        a = s.createElement(o),
                                m = s.getElementsByTagName(o)[0];
                        a.async = 1;
                        a.src = g;
                        m.parentNode.insertBefore(a, m)
                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                    ga('create', 'UA-68941120-1', 'auto');
                    ga('send', 'pageview');

        </script>
        <script>
                    $(function () {
                        $('a[href*=#]:not([href=#])').click(function () {
                            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                                var target = $(this.hash);
                                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                                if (target.length) {
                                    $('html,body').animate({
                                        scrollTop: target.offset().top
                                    }, 1000);
                                    return false;
                                }
                            }

                        });
                    });
        </script>
        <div class="modal fade" id="slModal" tabindex="-1" role="dialog" aria-labelledby="slModal-Quote">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="slModalTitle">G E T  A  Q U O T E </h4>
                    </div>
                    <div class="modal-body">
                        <?php include 'views/messenger.php'; ?>
                    </div> 
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>
        <script>
                    // Can also be used with $(document).ready()
                    $(window).load(function () {
                          $('.flexslider').flexslider({
                                animation: "slide"
                          });
                    });
        </script>  
        <div class="modal fade" id="slider-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><img src="/img/text.svg"></h4>
                    </div>
                    <div class="modal-body">
                        <span role="header" class="text-center formHeader text-nowrap"> SEND US A <br>
                            <i class="fa fa-envelope fa-3x hvr-icon-buzz-out"></i>
                        </span>
                        <br>
                        <img class="slLineBreak "src="img/lineBreak.png">
                        <div id="form-messages"></div>
                        <form class="center-block" id="ajax-contact" method="post" action="/backend/formValidator.php" >
                            <h3>Contact Information</h3>

                            <i class="fa fa-user fa-4x"></i>
                            <br>
                            <span class="form-inline">
                                <div class="form-group">
                                    <br>
                                    <input type="text" class="form-control" name="firstName"  size="40" required placeholder="First Name">
                                </div>
                                <div class="form-group">
                                    <br>
                                    <input type="text" class="form-control" name="lastName"  size="40"placeholder="Last Name">
                                </div>
                                <span class="error"><?php echo $nameErr; ?></span>
                                <br>
                            </span>
                            <br>
                            <span class="">
                                <i class="fa fa-send fa-4x"></i>
                                <br>
                                <div class="form-group">
                                    <label for="slInputEmail">
                                    </label>
                                    <br>
                                    <input ng-pattern="/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i" type="email" class="form-control" name="email" id="slInputEmail"  placeholder="Y O U R @ E M A I L . C O M">
                                    <br>
                                </div>
                                <i class="fa fa-mobile fa-5x "></i>
                                <br>



                                <div class="form-group">

                                    <input ng-model="val" ng-pattern="/^\d+$/" name="anim" class="my-input"
                                           aria-describedby="inputDescription"type="number" class="form-control" id="slInputPhone" name="phone"  placeholder="5 5 5 - 8 6 7  - 5 3 0 9">
                                </div>
                                <span class="error"> <?php echo $nameErr; ?></span>
                                <br><br>
                            </span>
                            <br>

                            <span class="form-inline">

                                <br>

                                <i class="fa fa-pencil fa-3x"></i>
                                <br>
                                <textarea name="message" rows="8" cols="90" placeholder="A brief discription on how we may help you."><?php echo $comment; ?></textarea>
                                <button id="locationToggleBtn" type="button" onclick="getLocation()" class="btn btn-danger" >Add Your Location <i class="fa fa-map-marker fa-5x"></i></button>

                            </span>

                            <br><br>

                            <br><br>
                            <input type="submit" class="btn btn-primary slSubmitFormBtn" name="submit" value="Submit"> 
                            <input type="reset" class="btn btn-info" name="reset" value="reset"> 
                            <input type="hidden" id='geoLocationInput' name='geoLocation' >
                            <input type="hidden" name="messageSubmit" value="Submit">

                        </form>
                        <div id='map'> </div>
                        <script>

                                    // Note: This example requires that you consent to location sharing when
                                    // prompted by your browser. If you see the error "The Geolocation service
                                    // failed.", it means you probably did not give permission for the browser to
                                    // locate you.



                                    var slGPSMessage = document.getElementById("geoLocationMessage");
                                    var slGPSInput = document.getElementById("geoLocationInput");
                                            function getLocation() {
                                                if (navigator.geolocation) {
                                                    navigator.geolocation.getCurrentPosition(showPosition);
                                                } else {
                                                    slGPSMessage.innerHTML = "Geolocation is not supported by this browser.";
                                                }
                                            }

                                    function showPosition(position) {
                                        slGPSMessage.innerHTML = "Latitude: " + position.coords.latitude +
                                                "<br>Longitude: " + position.coords.longitude;
                                        slGPSInput.setAttribute("value", "https://www.google.com/maps/?q=" + position.coords.latitude +
                                                "," + position.coords.longitude);
                                    }





                        </script>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <script src="/js/smilingL-parralaxScroll.js" type="text/javascript"></script>
    </body>
</html>
