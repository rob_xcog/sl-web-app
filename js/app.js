/* 
 * Copyright 2015 Simple Design <Rob.Xcog at xcogstudios@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var slApp = angular.module('slApp', []);


slApp.controller('sl-serviceCtrl', ['$scope', '$http', function ($scope, $http) {
        $http.get('backend/db/bases/services.json').success(function (data) {
            $scope.services = data;
        });
    }]);

slApp.controller('sl-quotesCtrl', ['$scope', '$http', function ($scope, $http) {
        $http.get('backend/db/bases/quotes.json').success(function (data) {

            $scope.quotes = data;
        });
    }]);

slApp.controller('sl-sliderCtrl', ['$scope', '$http', function ($scope, $http) {
                $http.get('backend/db/bases/slides.json').success(function (data) {
            $scope.slides = data;

        });

    }]);
   
  slApp.controller('splashCtrl', ['$scope', function ($scope){
           $scope.btnClick = function ($target) { 
               
               
document.getElementById($target).className = "active";  
           };
   }]);
  